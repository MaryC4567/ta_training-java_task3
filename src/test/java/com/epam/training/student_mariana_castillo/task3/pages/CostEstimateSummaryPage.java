package com.epam.training.student_mariana_castillo.task3.pages;

import com.epam.training.student_mariana_castillo.task3.utils.basePage.BasePage;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.List;

public class CostEstimateSummaryPage extends BasePage {

    public CostEstimateSummaryPage(WebDriver driver) {
        super(driver);
    }
    private final String summaryElement = "//*[@class='EQCBxd g5Ano']/span/span[1]/span[1]";
    private final String summaryOption= "//*[@class='EQCBxd g5Ano']/span/span[1]/span[2]";
    private final List<WebElement> summaryElements = driver.findElements(By.xpath(summaryElement));
    private final List<WebElement> summaryElementOption = driver.findElements(By.xpath(summaryOption));

    private WebElement summaryInstances, summaryOperatingSystem, summaryProvisioningModel, summaryMachineType, summaryAddGPUs,
            summaryGPUModel, summaryNumberOfGPUs, summaryLocalSSD, summaryRegion, summaryCommittedTerm;

    public void switchCase(){
        for (int i = 0; i<summaryElements.size(); i++) {
            switch (summaryElements.get(i).getText()){
                case "Number of Instances":
                    summaryInstances = summaryElementOption.get(i);
                    break;
                case "Operating System / Software":
                    summaryOperatingSystem = summaryElementOption.get(i);
                    break;
                case "Provisioning Model":
                    summaryProvisioningModel = summaryElementOption.get(i);
                    break;
                case "Machine type":
                    summaryMachineType = summaryElementOption.get(i);
                    break;
                case "Add GPUs":
                    summaryAddGPUs = summaryElementOption.get(i);
                    break;
                case "GPU Model":
                    summaryGPUModel = summaryElementOption.get(i);
                    break;
                case "Number of GPUs":
                    summaryNumberOfGPUs = summaryElementOption.get(i);
                    break;
                case "Local SSD":
                    summaryLocalSSD = summaryElementOption.get(i);
                    break;
                case "Region":
                    summaryRegion = summaryElementOption.get(i);
                    break;
                case "Committed use discount options":
                    summaryCommittedTerm = summaryElementOption.get(i);
                    break;
                default:
                    break;
            }
        }
    }

    public boolean numberOfInstances(String instances){
        return summaryInstances.getText().equalsIgnoreCase(instances);
    }
    public boolean operatingSystem(String operatingSystem){
        return summaryOperatingSystem.getText().equalsIgnoreCase(operatingSystem);
    }
    public boolean provisioningModel(String provisioningModel){
        return summaryProvisioningModel.getText().equalsIgnoreCase(provisioningModel);
    }
    public boolean machineType(String machineType){
        return summaryMachineType.getText().equalsIgnoreCase(machineType);
    }
    public boolean addGPUsStatus(String addGPUsOption){
        return summaryAddGPUs.getText().equalsIgnoreCase(addGPUsOption);
    }
    public boolean gpuType(String gpuTypeOption){
        return summaryGPUModel.getText().equalsIgnoreCase(gpuTypeOption);
    }
    public boolean numberOfGPUsSelected(String gpuNumber){
        return summaryNumberOfGPUs.getText().equalsIgnoreCase(gpuNumber);
    }
    public boolean localSSDCapacity(String ssdOption){
        return summaryLocalSSD.getText().equalsIgnoreCase(ssdOption);
    }
    public boolean location(String locationOption){
        return summaryRegion.getText().equalsIgnoreCase(locationOption);
    }
    public boolean committedUseDiscount(String committedTerm){
        return summaryCommittedTerm.getText().equalsIgnoreCase(committedTerm);
    }
}
