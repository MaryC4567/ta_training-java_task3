package com.epam.training.student_mariana_castillo.task3.pages;

import com.epam.training.student_mariana_castillo.task3.utils.basePage.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class HomePage extends BasePage {

    public HomePage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@class='mb2a7b']")
    private WebElement searchButton;

    @FindBy(xpath = "//a[@data-ctorig='https://cloud.google.com/products/calculator']")
    private WebElement googleCloudPricing;

    public PricingCalculatorPage searchPricingCalculator(String search){

        searchButton.click();
        searchButton.sendKeys(search);
        searchButton.sendKeys(Keys.ENTER);
        googleCloudPricing.click();

        return new PricingCalculatorPage(super.getDriver());
    }
}
