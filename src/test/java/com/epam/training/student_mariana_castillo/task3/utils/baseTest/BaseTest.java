package com.epam.training.student_mariana_castillo.task3.utils.baseTest;

import com.epam.training.student_mariana_castillo.task3.pages.HomePage;
import com.epam.training.student_mariana_castillo.task3.utils.Driver;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;

public abstract class BaseTest {

    static Driver driver;

    @BeforeAll
    public static void openUrl() {
        driver = new Driver();
        navigateTo("https://cloud.google.com/");
    }

    public static void navigateTo(String url) {
        driver.getDriver().get(url);
    }

    public HomePage loadFirstPage() {
        return new HomePage(driver.getDriver());
    }

    @AfterAll
    public static void closeDriver() {
        driver.getDriver().quit();
    }
}
