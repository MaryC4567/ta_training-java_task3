package com.epam.training.student_mariana_castillo.task3.pages;

import com.epam.training.student_mariana_castillo.task3.utils.basePage.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import java.util.List;

public class PricingCalculatorPage extends BasePage {

    public PricingCalculatorPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@class='UywwFc-vQzf8d']")
    private WebElement addToEstimate;
    @FindBy(xpath = "//*[@class='d5NbRd-EScbFb-JIbuQc PtwYlf']")
    private WebElement computeEngine;
    @FindBy(xpath = "//*[@id='c11']")
    private WebElement instancesNumber;
    @FindBy(xpath = "//span[@id='c22']//following-sibling::div")
    private WebElement operatingSystem;
    @FindBy(xpath = "//*[@data-value='free-debian-centos-coreos-ubuntu-or-byol-bring-your-own-license']")
    private WebElement operatingSystemOption;
    @FindBy(xpath = "//*[@class='zT2df' and @for='regular']")
    private WebElement regularProvisioningModel;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[1]")
    private WebElement machineFamily;
    @FindBy(xpath = "//*[@data-value='general-purpose']")
    private WebElement generalPurpose;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[2]")
    private WebElement series;
    @FindBy(xpath = "//*[@role='option' and @data-value='n1']")
    private WebElement seriesOption;
    @FindBy(xpath = "//div[@class='LHK0xb KXFYXb']/div[3]")
    private WebElement machineType;
    @FindBy(xpath = "//*[@data-value='n1-standard-8']")
    private WebElement machineTypeOption;
    @FindBy(xpath = "//button[@aria-label='Add GPUs']/div[@class='eBlXUe-l6JLsf']/span[@class='eBlXUe-hywKDc']")
    private WebElement addGPUToggle;
    @FindBy(xpath = "//div[@class='VfPpkd-O1htCb VfPpkd-O1htCb-OWXEXe-INsAgc VfPpkd-O1htCb-OWXEXe-SfQLQb-M1Soyc-Bz112c FkS5nd']/div/div")
    private List<WebElement> gpuSettings;
    @FindBy(xpath = "//div[@class='qUa9tb'][23]/div/div/div/div/div/div/div")
    private WebElement gpuModel;
    @FindBy(xpath = "//li[@data-value='nvidia-tesla-v100']")
    private WebElement nvidiaTeslaV100;
    @FindBy(xpath = "//div[@class='qUa9tb'][24]/div/div/div/div/div/div/div")
    private WebElement numberOfGPUs;
    @FindBy(xpath = "//li[1][@data-value='1']")
    private WebElement oneGPU;
    @FindBy(xpath = "//div[@class='qUa9tb'][27]/div/div[1]")
    private WebElement localSSD;
    @FindBy(xpath = "//*[@aria-label='Local SSD']/li[3]")
    private WebElement localSSDOption;
    @FindBy(xpath = "//div[@class='qUa9tb'][29]/div/div/div/div/div/div/div")
    private WebElement region;
    @FindBy(xpath = "//*[@data-value='europe-west4']")
    private WebElement netherland;
    @FindBy(xpath = "//*[@class='qUa9tb'][31]/div/div/div[2]/div/div/div[2]")
    private WebElement committedUseDiscountOneYear;
    @FindBy(xpath = "//*[@class='nQSTsf AZgcHf']/div/div/div[2]/div/button/span[5]")
    private WebElement addToEstimateButton;
    @FindBy(xpath = "//*[@jsname='V67aGc' and @class='FOBRw-vQzf8d']")
    private WebElement shareButton;
    @FindBy(xpath = "//*[@class='v08BQe']/a")
    private WebElement openEstimateSummary;
    @FindBy(xpath = "//*[@class='message-container ']/span[2]")
    private WebElement chatBotCloseButton;

    private void startEstimation() {
        waitToBeClickable(addToEstimate);
        addToEstimate.click();
        waitToBeClickable(computeEngine);
        computeEngine.click();
    }

    private void setInstances(String instances){
        waitToBeClickable(instancesNumber);
        instancesNumber.sendKeys(Keys.DELETE);
        instancesNumber.sendKeys(instances);
    }

    private void selectOperatingSystem(){
        operatingSystem.click();
        operatingSystemOption.click();
    }

    private void setProvisioningModel(){
        regularProvisioningModel.click();
    }

    private void selectMachineFamily(){
        machineFamily.click();
        generalPurpose.click();
    }

    private void selectSeries(){
        series.click();
        waitToBeClickable(seriesOption);
        seriesOption.click();
    }

    private void selectMachineType(){
        machineType.click();
        machineTypeOption.click();
    }

    private void addGPUs(){
        addGPUToggle.click();
        waitToBeClickable(gpuModel);
        gpuModel.click();
        nvidiaTeslaV100.click();
        numberOfGPUs.click();
        oneGPU.click();
    }

    private void setLocalSSDData(){
        localSSD.click();
        localSSDOption.click();
    }

    private void selectRegion(){
        region.click();
        waitToBeClickable(netherland);
        netherland.click();
    }

    private void selectCommittedUseDiscount(){
        committedUseDiscountOneYear.click();
    }

    private void closeChatBot(){
        waitToBeClickable(chatBotCloseButton);
        chatBotCloseButton.click();
    }

    private void addToEstimatePopUp(){
        addToEstimateButton.click();
    }

    private void openEstimateSummary(){
        shareButton.click();
        waitToBeClickable(openEstimateSummary);
        openEstimateSummary.click();
    }

    public CostEstimateSummaryPage computeEnginePricing(String instances){
        startEstimation();
        setInstances(instances);
        selectOperatingSystem();
        setProvisioningModel();
        selectMachineFamily();
        selectSeries();
        selectMachineType();
        addGPUs();
        setLocalSSDData();
        selectRegion();
        selectCommittedUseDiscount();
        closeChatBot();
        addToEstimatePopUp();
        openEstimateSummary();
        for(String winHandle : driver.getWindowHandles()){
            driver.switchTo().window(winHandle);
        }
        return new CostEstimateSummaryPage(super.getDriver());
    }

}
