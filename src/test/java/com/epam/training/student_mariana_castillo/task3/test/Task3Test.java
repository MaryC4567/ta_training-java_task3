package com.epam.training.student_mariana_castillo.task3.test;

import com.epam.training.student_mariana_castillo.task3.pages.CostEstimateSummaryPage;
import com.epam.training.student_mariana_castillo.task3.pages.HomePage;
import com.epam.training.student_mariana_castillo.task3.pages.PricingCalculatorPage;
import com.epam.training.student_mariana_castillo.task3.utils.baseTest.BaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class Task3Test extends BaseTest {

    @Test
    public void computeEngineSummaryValidateData(){

        String search = "Google Cloud Platform Pricing Calculator", instances = "4",
                operatingSystem = "Free: Debian, CentOS, CoreOS, Ubuntu or BYOL (Bring Your Own License)",
                provisioningModel = "Regular", machineType = "n1-standard-8, vCPUs: 8, RAM: 30 GB",
                addGPUsOption = "true", gpuTypeOption = "NVIDIA Tesla V100", gpuNumber = "1", localSSDCapacity = "2x375 GB",
                locationOption = "Netherlands (europe-west4)", committedTerm = "1 year";

        HomePage home =loadFirstPage();
        PricingCalculatorPage pricing = home.searchPricingCalculator(search);
        CostEstimateSummaryPage summary = pricing.computeEnginePricing(instances);
        summary.switchCase();

        Assertions.assertTrue(summary.numberOfInstances(instances));
        Assertions.assertTrue(summary.operatingSystem(operatingSystem));
        Assertions.assertTrue(summary.provisioningModel(provisioningModel));
        Assertions.assertTrue(summary.addGPUsStatus(addGPUsOption));
        Assertions.assertTrue(summary.localSSDCapacity(localSSDCapacity));
        Assertions.assertTrue(summary.location(locationOption));
        Assertions.assertTrue(summary.committedUseDiscount(committedTerm));
        Assertions.assertTrue(summary.machineType(machineType));
        Assertions.assertTrue(summary.gpuType(gpuTypeOption));
        Assertions.assertTrue(summary.numberOfGPUsSelected(gpuNumber));

        System.out.println("Cost Estimate Summary created successfully");
        System.out.println("Data is correct");
    }
}


