# ta_training-java_Task3

## Overview
This repository contains tests for task3 for Google Cloud Platform Pricing Calculator https://cloud.google.com/products/calculator?hl=en 

## Details
Selenium 4.20.0 
Java  17.0.11
Junit 5.10.0

## Developed by
Mariana Castillo López